# CHIP-2022-05 Pay-to-Script-Hash-32 (P2SH32) for Bitcoin Cash

        Title: Pay-to-Script-Hash-32 (P2SH32) for Bitcoin Cash
        First Submission Date: 2022-05-30
        Owners: bitcoincashautist (ac-A60AB5450353F40E)
        Type: Technical
        Layers: Consensus, Script, Network
        Status: DRAFT
        Current Version: 1.6.1
        Last Edit Date: 2022-11-06

## Contents

<details><summary><strong>Table of Contents</strong></summary>

- [Summary](#summary)
- [Deployment](#deployment)
- [Motivation](#motivation)
- [Benefits](#benefits)
- [Technical Description](#technical-description)
- [Security Analysis](#security-analysis)
- [Implementations](#implementations)
- [Test Cases](#test-cases)
- [Activation Costs](#activation-costs)
- [Ongoing Costs](#ongoing-costs)
- [Costs and Benefits Summary](#costs-and-benefits-summary)
- [Risk Assessment](#risk-assessment)
- [Evaluation of Alternatives](#evaluation-of-alternatives)
- [Discussions](#discussions)
- [Statements](#statements)
- [Acknowledgements](#acknowledgements)
- [Changelog](#changelog)
- [Copyright](#copyright)

</details>

## Summary

[[Back to Contents](#contents)]

[This proposal](https://gitlab.com/0353F40E/p2sh32/-/blob/main/CHIP-2022-05_Pay-to-Script-Hash-32_(P2SH32)_for_Bitcoin_Cash.md#chip-2022-05-pay-to-script-hash-32-p2sh32-for-bitcoin-cash) describes an upgrade to Bitcoin Cash peer-to-peer electronic cash system that increases cryptographic security of existing Bitcoin Cash P2SH feature by enabling a 32-byte variant of it.

The currently used 20-byte variant has a known weakness against birthday attack.
Currently used applications are not affected but many other, decentralized applications are prevented from being created because they would be insecure or impractical.

The upgrade will be deployed as part of Bitcoin Cash upgrade schedule, that allows for clean upgrades using orderly hard-forks.
Only validating node software needs to be upgraded while other applications won't require an upgrade, meaning everyone can continue using Bitcoin Cash (BCH) as they're used to, no action required.
Upgrading old applications or creating new ones will be on a voluntarily basis by those that want to access the upgraded feature, and at their own pace.
We expect that many will be attracted to build and access products that will rely on the hardened, 32-byte variant of P2SH feature, and that such products will bring more utility to Bitcoin Cash.

## Deployment

[[Back to Contents](#contents)]

Deployment of this specification is proposed for the May 2023 upgrade.

## Motivation

[[Back to Contents](#contents)]

Feasibility of finding 160-bit hash collision against Bitcoin addresses has been understood since [circa 2016](https://lists.linuxfoundation.org/pipermail/bitcoin-dev/2016-January/012234.html), and [literature confirms](https://www.cs.umd.edu/~jkatz/imc/hash-erratum.pdf) that a constant-memory search for a collision in 2^(N/2) is possible by finding cycles.
Today, the aggregate of SHA-256 mining is running at roughly 200EH/s which translates to calculating about 2^83.8 SHA-256 hashes per day for roughly 27m USD worth of block rewards.
The cost per hash does not directly translate to function used for the addresses.
However, considering RIPEMD-160 and SHA-256 are of the same family, it can give us a ball-park estimate of what is possible with current chip-making technology: 4m USD for a HASH-160 collision.

Until recently, only multisig would be exposed and only in specific circumstances where an adversary would have a final say in shared address generation, and could convince his target to pay enough funds into the address to make the collision search pay off:

>If you are agreeing to lock up funds with somebody else, and they control what public key to use, you are susceptible to collision attacks.

Most current users are not exposed at all, and those that may be at risk could remove their risk by insisting on collision-safe address generation schemes when locking funds with others.

Recently the issue has been [brought to attention](https://bitcoincashresearch.org/t/p2sh32-a-long-term-solution-for-80-bit-p2sh-collision-attacks/750) in context of Bitcoin Cash blockchain, because it has expanded its Script capabilities.
Historically, Bitcoin contract language wasn't really used for anything more than multisig which was the main and probably only widespread case of "locking funds with somebody else".
Even that use-case is exploitable in some [con scenarios](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#example-attack).
Bitcoin Cash is continuing to evolve Bitcoin Script language, with the latest upgrade (May 2022) bringing much more versatility to Script language:

- [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md) and
- [CHIP-2021-03: Bigger Script Integers](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Bigger-Script-Integers.md).

For example, one of the first applications demoed is a [public crowdfunding assurance contract](https://bitcoincashresearch.org/t/p2sh-assurance-contract/720), but it is safe to assume that there will be many more applications.
Those uses could potentially attract enough users so that a con relying on the feasible P2SH collision would become attractive to adversaries.
Seemingly legitimate contracts could intentionally be set-up as insecure in an elaborate con like what was [described here](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#example-attack).
During the set-up phase of the con, an adversary would essentially generate a multisig address with a backdoor that would allow him alone to spend it all at the right moment.

Multisig address generation happens only once, and from then on it is just used, so there is only the one opportunity for an attack and the risk can be mitigated by using an interactive key-exchange that ensures no party has a chance to "re-roll" the final address in a collision search.

What if address generation happens on every spend from the contract?
Attack scenarios will depend on how they are set up.
The contract may require authentication by a key or a set of keys, or it may be a public covenant where anyone could spend from it provided he updates the contract state as required.
In either case, if the contract allows enough entropy to be fed into its internal state when used, then every update is an opportunity for a collision search and the risk of total loss of funds will be there for as long as the contract lives.

Some examples of the above include:

1. Contract that raise money from the public and make promises about how the funds will be spent.
These are designed to ensure that the promise is kept, like releasing funds on fixed intervals.
Collision-search for an alternative contract would allow the trustee to break out from self-imposed limitations.
2. Contracts that manage funds for an organization, allowing some keys to take only a limited amount of funds.
These could be exposed to a collision-search by a rogue employee if the organization's treasury would grow big enough.
3. Oracle-dependent contracts, where releasing the funds to some party in the contract is dependent on external data provided by an oracle.
If it is possible to manipulate how the oracle is read from by the contract, then it may open an opportunity for a collision-search.
4. Covenants used to create bridges with other blockchains, which could end up locking a significant amount of funds and become attractive targets for adversaries.
5. DAO-like contracts, where anyone is free to interact with the funds locked inside of it while being allowed to update some internal state of the DAO.
These would be exposed to a collision attack by anyone at any time, and a success would allow them to break out from the DAO and simply take all the funds.

Above contracts would be extremely hard to audit because entropy for a collision may be obscurely snuck-in.
It would be hard or even impossible to set-up those kinds of contracts in a way that would mitigate the risk.
If the enclosing hash function for these contracts was secure against collision-attacks, then all of these problems would simply disappear.

## Benefits

[[Back to Contents](#contents)]

Instead of application developers having to work around these issues, the vulnerability can be resolved at consensus layer as part of orderly hard-for upgrade and without any new costs to the ecosystem.
This would allow faster and easier development of new products and application on Bitcoin Cash network that would benefit the whole ecosystem, either directly or indirectly.

Overall, Bitcoin Cash smart contracts will be more secure since their security against the collision attack will not have to depend on getting the workarounds right.
It will be easier to both develop and audit secure contracts offered to the public.
Contract usage will be simpler too, because workarounds typically involve pre-commitment schemes which require interaction whereas with the secure variant users can simply just do the familiar "send to address".

## Technical Description

[[Back to Contents](#contents)]

The mechanics are the same as current P2SH: if an output's locking script matches a specific ["hashlock"](https://en.bitcoin.it/wiki/Hashlock) pattern (`OP_HASH160 <data> OP_EQUAL`) then the input's top stack element will get executed as redeem script.
With the upgrade, one more output locking script "hashlock" pattern (`OP_HASH256 <data> OP_EQUAL`) will be activating the same mechanics.

For readers looking for more information and background on the P2SH feature, a broad ["everything P2SH"](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#bitcoin-cash-node-technical-bulletin) document has been created in advance of this CHIP.

### Consensus Rules

This upgrade will recognize an **additional** output locking script pattern that will activate P2SH consensus logic just like the existing BIP-0016 pattern.
After the upgrade, there will be two possible ways to lock-up funds in a P2SH contract:

Existing, P2SH20:

- Locking script:  
`OP_HASH160 OP_DATA_20 hash160(redeem_script) OP_EQUAL`  
(raw: `a9` `14` `1122334455667788990011223344556677889900` `87`).

Proposed, P2SH32:

- Locking script:  
`OP_HASH256 OP_DATA_32 hash256(redeem_script) OP_EQUAL`  
(raw: `aa` `20` `1122334455667788990011223344556677889900112233445566778899001122` `87`).

Both output types will be unlocked the same way:

- Unlocking script:  
`[push data N] ... [push data 1] [push data 0] <push redeem_script>`.

It will pe possible to have instances of the same contract as both P2SH20 and P2SH32.
The choice of hash is at user's discretion, depending whether his use-case requires "hardened" security assumptions.

#### Activation

The upgrade is by nature a soft-fork, but as part of orderly Bitcoin Cash upgrade cycle it will be rolled out as a hard-fork using the median-time-past (MTP) activation strategy which intentionally breaks compatibility with non-upgraded nodes.

It is possible that, around activation time, a node's mempool will have some transactions spending from P2SH32 pattern using old rules.
Node implementations must make sure to purge the mempool when activation MTP will be reached, to avoid having these old transactions linger in the mempool or worse - cause mining failure in case of mining nodes attempting to include invalid transactions in their blocks.

#### Validating Historical Transactions

As of time of this writing (block 742020) there exist both spent and unspent transaction outputs matching the proposed P2SH32 output pattern.
Situation was similar at the time when BIP-0016 was proposed:

>These new rules should only be applied when validating transactions in blocks with timestamps >= 1333238400 (Apr 1 2012) [1]. There are transactions earlier than 1333238400 in the block chain that fail these new validation rules. [2]. Older transactions must be validated under the old rules. (see the Backwards Compatibility section for details). 

The same would apply for activating the new P2SH32 output pattern: older transactions must be validated under the old rules.
For information, [here](https://gitlab.com/0353F40E/announcements/-/blob/p2sh-data/attachments/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future/p2sh_data_EN.md#op_hash256) is a list of all existing transaction outputs, both spent and unspent, which match the proposed P2SH32 pattern.

#### Segwit Recovery

Bitcoin Cash May 2019 upgrade introduced an exception for the P2SH pattern in order to allow miners to [recover funds](https://documentation.cash/protocol/forks/hf-20190515#allow-segwit-recovery) wrongly sent to Segwit addresses on Bitcoin Cash network.
More background is given [here](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#471-interaction-with-bitcoin-core-btc).

With the 32-byte P2SH variant, no such exception is required, because P2SH32 does not exist on Bitcoin Core network, and its [32-byte P2WSH](https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki#p2wsh) variant uses a pattern distinct from what is proposed here.
As such, the special exception for P2SH segwit recovery **must not** apply to P2SH32 locking scripts.

### Network Rules

When it comes to [network-level validation rules](https://documentation.cash/protocol/blockchain/transaction-validation/network-level-validation-rules), it is only required to update the [standard locking script](https://documentation.cash/protocol/blockchain/transaction/locking-script) rule to recognize the P2SH32 locking script pattern.
We will summarize standard outputs below:

- P2PK (existing, deprecated): `<key> OP_CHECKSIG`
- P2PKH (existing): `OP_DUP OP_HASH160 <hash_20> OP_EQUALVERIFY OP_CHECKSIG`
- P2MS (existing): `<m><<key 1>...<key n>><n> OP_CHECKMULTISIG`
- OP_RETURN (existing): `OP_RETURN [data 0]...[data N]`
- P2SH20 (existing): `OP_HASH160 <hash_20> OP_EQUAL`
- **P2SH32 (new): `OP_HASH256 <hash_32> OP_EQUAL`**

Other than that, no change is required.

### Wallet Address Format

Not all users will have a need to spend from P2SH contracts, but many may want to be able to send to them (example: [crowdfunding contract](https://bitcoincashresearch.org/t/p2sh-assurance-contract/720)).
The current [address standard](https://github.com/bitcoincashorg/bitcoincash.org/blob/master/spec/cashaddr.md#version-byte) already supports extending P2SH addresses [up to 64 bytes](https://github.com/bitcoincashorg/bitcoincash.org/blob/master/spec/cashaddr.md#larger-test-vectors).
To allow users to easily send Bitcoin Cash to P2SH contracts secured with a 32-byte hash, wallets should recognize longer addresses.

## Security Analysis

[[Back to Contents](#contents)]

The upgrade uses only the existing primitives in the same way BIP-0016 did, and relies on the "main" Bitcoin hash function (double SHA-256) for cryptographic security.
Therefore we posit that it doesn't introduce any new DoS vectors or vulnerabilities.
For a more in-depth analysis of P2SH cryptographic security, please see the linked document sections ["Cryptographic Security Level"](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#3-cryptographic-security-level) and ["Preferred Upgrade"](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#4-preferred-upgrade).

## Privacy Analysis

[[Back to Contents](#contents)]

We can identify 4 major categories of future P2SH32 users:

1. Users of transparent contracts that would need to use P2SH32 in place of P2SH20.
Such contracts could anyway be eliminated from P2SH anonymity set by analyzing the inputs that reveal the redeem scripts, so moving them from P2SH20 to P2SH32 outputs would not reduce the anonymity set of P2SH20 outputs.
2. Users of opaque contracts and NOT at risk from collision attack would have 0 incentive to switch to P2SH32 and would continue to increase and benefit from the P2SH20 anonymity set.
3. Users of opaque contracts and at risk from collision attack but viable as both risk-mitigated P2SH20 and P2SH32 will have freedom in deciding which trade-off they prefer:
    - More interactive address generation (e.g. multisig with pre-commitment key-exchange scheme), smaller transaction sizes, and bigger anonymity set.
    - Less interactive or non-interactive address generation (e.g. multisig with simple key-exchange, or using already known keys), bigger transaction sizes, and smaller anonymity set.
4. Users of opaque contracts and at risk from collision attack but not viable as P2SH20 would have no choice but to accept the initially small anonymity set. These users would not take from the P2SH20 anonymity set and would benefit future users of P2SH32 by slowly growing the set of opaque P2SH32 UTXOs.

We don't expect a sudden change in multisig user patterns, especially since there will be no urgency to support P2SH32 by existing popular applications.
We expect first uses of P2SH32 to fall into category 1., and their use facilitated by newly written application-specific software, therefore we see privacy impact to existing users as insignificant.

## Other Considerations

[[Back to Contents](#contents)]

### Pay-to-Public-Key-Hash (P2PKH) Outputs

Outputs using Pay-to-Public-Key-Hash (P2PKH) locking script pattern are authenticating their public key using a 20-byte hash.
They are generally not exposed to a collision attack because normally there will be a single owner for the address and a collision would merely give the owner two ways to spend their own funds.
Therefore, increasing the hash size of P2PKH outputs to 32 bytes is generally not necessary.
Even if it were, users could have their keys [wrapped by P2SH32](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#7-pay-to-public-key-hash-p2pkh-outputs) so there will be no need for a 32-byte variant of P2PKH.

Note that it is possible to construct a key where multiple parties only hold shares of a shared single key.
Such cryptographic scheme is called a [threshold signature scheme](https://eprint.iacr.org/2020/1390.pdf):

>A *threshold signature scheme (TSS)* enables a group
of parties to collectively compute a signature without
learning information about the private key. In a (*t*, *n*)-
threshold signature scheme, n parties hold distinct key
shares and any subset of *t* + 1 ≤ *n* distinct parties can
issue a valid signature, whereas aby subset of t or fewer
parties can’t. TSS’ setup phase relies on *distributed key
generation (DKG)* protocol, whereby the parties generate
shares without exposing the key. In practice, TSS is often
augmented with a *reshare protocol* (a.k.a. share rotation),
to periodically update the shares without changing the
corresponding key.

Users of these signatures could choose between using:

1. P2PKH with a pre-commitment scheme
2. [P2PK nested inside P2SH32 (P2PKSH32)](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#7-pay-to-public-key-hash-p2pkh-outputs).

Using a threshold signature scheme over multisig would have some savings in transaction sizes but the main motivation would be privacy.
Therefore, it is expected that those users would have no incentive to switch to P2SH32, especially since distributed key generation protocols (DKGs) anyway have to guard against malicious parties manipulating the resulting shared public key, a risk which P2SH32 would not remove.

### Interaction With Derivatives of Bitcoin Cash

As discussed [here](https://bitcoincashresearch.org/t/p2sh32-a-long-term-solution-for-80-bit-p2sh-collision-attacks/750/3), replays of some transactions sending unsplit funds to P2SH32 may be possible on derivatives of Bitcoin Cash.
Users of P2SH32 should therefore be careful not to use unsplit funds.
On those derivatives, funds sent to P2SH32 outputs would be free for the taking because there they would be seen as hashlock contracts.
Miners could easily donate such funds to themselves because they could learn the unlocking preimage by looking at Bitcoin Cash blockchain.
The P2SH32 pattern is not recognized as standard by other chains network rules, so those outputs could only be claimed by miners.

This is not seen as a problem which needs addressing by Bitcoin Cash network because those cases should be extremely rare since amount of unsplit funds only decreases with time.
Besides, it is the responsibility of each network to safeguard their own users funds.

## Implementations

[[Back to Contents](#contents)]

- Bitcoin Cash Node
  - [Merge Request #1556 - Implement P2SH_32](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1556)
  - [Merge Request #1600 - Upgrade9 (May 2023) - Everything plus fixups](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1600)

## Test Cases

[[Back to Contents](#contents)]

P2SH32 test cases are [included as part of CHIP-2022-02-CashTokens: Token Primitives for Bitcoin Cash](https://github.com/bitjson/cashtokens#transaction-validation-test-vectors) and are implemented in [Merge Request #1600 - Upgrade9 (May 2023) - Everything plus fixups](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1600).

## Activation Costs

[[Back to Contents](#contents)]

We define activation cost as any cost that would arise ex-ante, in anticipation of the activation by affected stakeholders, to ensure that their services will continue to function without interruption once the activation block will be mined.
In case of this proposal, activation cost is contained to nodes and will amount to:

- Some fixed amount of node developer man-hours, in order to release updated node software that will correctly validate transactions implementing the new consensus specification.
- Some fixed amount of work by stakeholders running the node software, in order to deploy the software in anticipation of hard-fork activation.
- Some fixed amount of effort by others involved in reviewing and testing the new node software version.

This is the same kind of cost that any hard-fork upgrade has had or will have.
Nothing is required of stakeholders who are not running validating nodes or involved in node development.

## Ongoing Costs

[[Back to Contents](#contents)]

We define ongoing cost as any cost that would arise post upgrade.
They could be simply an addition to node operating costs, or development costs to support new features that would become viable post activation.
Here we have to make an important distinction, and we will define two broad categories:

1. Unavoidable costs, such as node operating costs required to validate new rules.
2. Opt-in costs, such as block explorer upgrade and operating costs to support decentralized applications.

We don't see a significant increase in unavoidable costs and on that we will elaborate below.
With opt-in costs, it is reasonable to expect that those will be part of some economic calculation which would result in a net benefit.
We will discuss both nevertheless.

### Unavoidable Ongoing Costs

These will be contained to node software, and consist of:

- Node software maintenance costs.
- Node operating costs.

#### Node Software Maintenance Costs

The proposal is an extension of an existing feature using a simple change to consensus specification, it is conceptually simple.
When it comes to implementation, it is not trivial to implement because it affects many parts of the codebase, as we can observe in the proof-of-concept implementation.
However, it is easy to reason about and we do not see this proposal as introducing potential surprises to those who would later maintain the code.

#### Node Operating Costs

Impact on node operating costs can be analyzed through:

1. Network bandwidth requirements.
2. Node local storage requirements.
3. Node CPU time required for transaction validation.

##### Additional network bandwidth requirements

The 32-byte P2SH output variant is 12 bytes more expensive than the 20-byte one, while input size is the same.

When discussing transaction sizes, this is no different than some custom input script, increasing the number of multisig keys, or OP_RETURN protocols.
The user who has a need for the extra feature has to add some more data to the transaction which encodes his use of the feature, and for that he pays the miner's fee.
Difference is in that P2SH hashes are stored in the "premium" part of the blockchain, the UTXO set, which we will discuss below.

##### Node local storage requirements

When a node performs transaction validation then, in addition to the data provided by the transaction itself, it must also fetch each referenced UTXO from the database so that it can access previous output's satoshi amount and locking script.
This holds the same for both "bare" (P2PKH and P2MS) and P2SH BCH outputs.
No new data structures are required, and no additional database access operations are required.

It is only the size of the `lockingScript` field which is increased, and that field is part of the UTXO set, for which nodes already build and maintain an index.
Consensus rules already support `lockingScript` sizes of variable length, and even network rules allow a variable length with the P2MS output template.

##### Node CPU time required for transaction validation

Both the current HASH-160 and the upgraded HASH-256 are implemented as composite hash functions, and in both cases a round of SHA-256 is performed first.
In other words, bulk of the work is performed by the inner SHA-256, to digest the full message into intermediate hash.

For example, double hashing a 300-byte redeem script has 300 bytes pass through the inner hash function, and 32 through the outer, so even if the cost of RIPEMD-160 was 0 that would result in only 10% cost difference for the example script.
For small scripts, the relative cost difference would be bigger, but even so, it would be small compared to the cost of verifying signatures and hashing the whole transaction for the transaction identifier.

Additionally, the upgrade would slightly reduce CPU operations density for transactions, because a transaction can fit a smaller number of bigger outputs.

### Opt-in Ongoing Costs

These can be thought of as cost of accessing the new features.
For example, a wallet doesn't need to support new features and cost to that wallet's developers and users will be 0 unless they want to access the new features.

#### P2PKH, P2MS, and P2SH20 Users

No action will be required by those that will be using and interacting only with existing applications.
If they will have a need to send to P2SH32 outputs, then wallets would have to support sending to P2SH32 addresses.
Thankfully, current standard supports bigger addresses so those already implementing [the standard](https://github.com/bitcoincashorg/bitcoincash.org/blob/master/spec/cashaddr.md) should be ready to support sending to P2SH32 addresses.

It should be noted here that a key property of birthday-hardened P2SH use-cases is that they are likely to be interact with users through specialized web or local wallets anyway, and are unlikely to require direct depositing into contracts from common wallets.
Therefore the usefulness of these use-cases, and by extension P2SH32 itself, is not dependent on widespread adoption of P2SH32 among common wallets and software.

#### P2SH32 Users

Developing new Script applications will of course require updates to some wallets, libraries, transaction compilers, and other software which implements the Bitcoin Cash Script VM (e.g. Bitauth IDE).

Secure treasury management setups relying on P2SH32 contracts may also require upgraded hardware wallet firmware, or even tailored custom firmware.

#### SPV Servers

Even without a change to SPV servers, upgraded wallets could still access the new feature by subscribing to the `lockingScript` as it would return both P2SH patterns verbatim.

#### Other Software

Block explorers, exchanges, indexers, etc. may be running some custom node-dependent software.
While this upgrade wouldn't break their existing functionality, accessing the new feature (like exchanges allowing withdrawals directly to P2SH32) would likely require an upgrade of their software stack.

## Costs and Benefits Summary

[[Back to Contents](#contents)]

General costs and benefits:

- (+) Improvement in overall security of Bitcoin Cash smart contracts.
- (+) Enabling more efficient UTXO powered decentralized applications.
- (+) Potentially attracting new users, talent, and capital thus helping grow the entire ecosystem centered around Bitcoin Cash.

### 1 Node Stakeholders

Includes both node developers and those running node software.

- (-) Generic costs of upgrading node software for Bitcoin Cash hard-fork upgrades.
- (0) Practically no impact on node operating costs.

#### 1.1 Miners

- (0) No costs other than general.

#### 1.2 Big nodes

Exchanges, block explorers, SPV servers, etc.

- (0) No costs other than general.

#### 1.3 User nodes

- (0) No costs other than general.

#### 1.4 Node developers

- (-) One-off workload increase to implement this proposal.
- (-) Codebase maintenance.

### 2 Userspace stakeholders

- (+) Lowered barrier to entry for accessing UTXO-based smart contracts technology.

#### 2.1 Business users

- (+) Access products and features previously not available on Bitcoin Cash network.
- (+) Enables potential new business models.

#### 2.2 Userspace developers

- (+) With more growth there will be more opportunity for professional advancement within the ecosystem.

##### 2.2.1 Wallet Developers

- (-) One-off workload increase to support sending to P2SH32 addresses.

##### 2.2.2 Library Developers

- (-) One-off workload increase to support spending from P2SH32 outputs.

##### 2.2.3 Contract Developers

- (+) Possibility of writing collision-safe contracts without expensive and cumbersome workarounds.

#### 2.3 Individual users

- (+) Get to enjoy the best peer-to-peer electronic cash system!
- (+) Safer from the risk of sending funds to a backdoored contract: simply insist on a 32-byte P2SH address.

### 3 Holders and speculators

#### 3.1 Long

- (+) Adoption correlates with increase in price and liquidity.

#### 3.2 Short

- (-) Adoption correlates with increase in price.
- (+) Adoption correlates with increase in liquidity.

### 4 Opinion holders who are not stakeholders

- (0) We are hoping developments like this will move them towards becoming stakeholders!

## Risk Assessment

[[Back to Contents](#contents)]

If we define risk as [probability times severity](https://en.wikipedia.org/wiki/Risk#Expected_values) then we could make an estimate using the [risk matrix](https://en.wikipedia.org/wiki/Risk_matrix).

Generic consensus upgrade risks apply.
Probability of bugs and network splits is entirely controlled by node developers and quality of their work.
After the node software QC process is completed, the probability of a bug is expected to be very low.

### Wrong Wallet Implementation

If a wallet would read a P2SH32 address, we can imagine that a wrong implementation may construct an output that uses the OP_HASH160 (0xa9) function instead of OP_HASH256 (0xaa):

- Locking script:  
**`OP_HASH160`** `OP_DATA_32 hash256(redeem_script) OP_EQUAL`  
(raw: **`a9`** `20` `1122334455667788990011223344556677889900112233445566778899001122` `87`).

This output would be valid from consensus point of view, but it would be provably unspendable since the `OP_EQUAL` could never evaluate to true.

Consequence of having such a transaction mined would be a loss of funds for user(s).

Probability of an user getting a transaction like that mined is very low, because of network rules.
Such transaction would never be relayed by nodes, so it could only be mined if the user somehow sent it directly to a miner.

It is more likely that the transaction would simply fail, and eventually the bug would be caught and resolved by the wallet resulting only in temporary downtime for users wanting to send to P2SH32 addresses.

### Historical Outputs Becoming Unspendable

Unspent funds currently locked in outputs with the OP_HASH256 hashlock pattern would almost certainly be made unspendable (if not already) with the upgrade.
As of time of this writing (block 742020) the total amount of [such outputs](https://gitlab.com/0353F40E/announcements/-/blob/p2sh-data/attachments/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future/p2sh_data_EN.md#op_hash256) is 0.00740026 BCH so currently this is not seen as a problem that would justify making exceptions to have them spendable as hashlock contracts under old rules.

## Evaluation of Alternatives

[[Back to Contents](#contents)]

### Transaction Encoding

More efficient encoding would be possible using a [dedicated opcode](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#61-transaction-encoding) such as:

- locking script:  
`OP_P2SH32 hash256(redeem_script)`,  

or inserting a dedicated transaction output field using the [PreFiX method](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2021-12-23_evaluate_viability_of_transaction_format_or_id_change_EN.md#an-alternative-to-breaking-change) such as:

- locking script:  
`PFX_P2SH32 hash256(redeem_script)`,

which would also enable the P2SH feature to be [separated from the Script VM layer](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#62-mixing-layers).

Both would be a hard-forking change by nature because non-upgraded nodes would see the outputs as using disabled opcodes, but they still wouldn't break non-node software.
However, implementation costs for that other software would be slightly higher as it would need to implement another distinct output template.

Rolling out this upgrade with the already used encoding template will keep the costs and risks to a minimum while solving the collision problem and enabling advanced UTXO contract features.

This upgrade doesn't preclude a future and more broad transaction format upgrade that would optimize all common output patterns.

### OP_SHA256

Current Script VM opcodes leave us with two possibilities for a 256-bit hash function: OP_SHA256 and OP_HASH256.
The `OP_SHA256` is self-descriptive, and the `OP_HASH256` being proposed is a composite hash just like OP_HASH160 but, instead of using RIPEMD-160 as the outer function, it is using the same SHA-256 function as the outer function:

- `hash256(x) = sha256(sha256(x))`.

The composite is commonly referred to as "double-SHA256" and it is the same hash function used as the ["main" hash](https://documentation.cash/protocol/blockchain/hash#sha-256) in Bitcoin design.

Either one would enable a 32-byte variant of the P2SH feature.
The main differences between two functions are:

- CPU cost of evaluating the function.
- Susceptibility to a length-extension attack.
- Composite (double SHA-256) vs non-composite (single SHA-256) function structure.
- Protocol consistency.

Please see [here](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#4-preferred-upgrade) for a more detailed analsyis of each difference.

The main trade-off for OP_HASH256 vs OP_SHA256 is stronger protocol consistency vs a micro-optimization, and speculative benefits add some secondary weight in favor of OP_HASH256.

### Other Hash Functions

We have not considered other 256-bit-output hash functions such as [BLAKE2s-256](https://en.wikipedia.org/wiki/BLAKE_(hash_function)#BLAKE2) or [SHA3-256](https://en.wikipedia.org/wiki/SHA-3#Comparison_of_SHA_functions), both which are secure against preimage, collision, and length-extension attacks, and may also offer an increase in transaction validation performance.
Using one of those would require either:

- upgrading the Script VM with a new opcode for the function or
- moving redeem script authentication [to the base layer](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#62-mixing-layers) and there using a hash function not available to the Script VM.

Introducing a new hash function would be a bigger intervention than what is required to address the collision problem we set out to do.
From security point of view and as far as we are aware: it would offer an improvement over our choice of double SHA-256 only in the exotic example of [hash chains used as PoW](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2022-06-09_bitcoin_cash_pay_to_script_hash_p2sh_past_present_and_future_EN.md#42-susceptibility-to-a-length-extension-attack), and for which we cannot demonstrate any applicability to the P2SH feature.
From performance point of view: it may offer a tempting improvement when viewed in isolation.
The costs and benefits of a new hash function should be carefully evaluated, and would be a more broad change, one which is beyond scope of this upgrade.

### Other Blockchains

Bitcoin Core (disambiguation) has already closed the vulnerability by introducing [BIP-0141](https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki) (SegWit) which enables 256-bit commitments to redeem scripts ([P2WSH](https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki#P2WSH)).
The BIP in question was one of the main reasons for the 2017 UAHF split, so naturally Bitcoin Cash would have to close the vulnerability in another way, like what is being proposed here.

## Discussions

[[Back to Contents](#contents)]

- [P2SH32: a long-term solution for 80-bit P2SH collision attacks](https://bitcoincashresearch.org/t/p2sh32-a-long-term-solution-for-80-bit-p2sh-collision-attacks/750)
- [CHIP-2022-05 Pay-to-Script-Hash-32 (P2SH32) for Bitcoin Cash](https://bitcoincashresearch.org/t/chip-2022-05-pay-to-script-hash-32-p2sh32-for-bitcoin-cash/806)

## Statements

[[Back to Contents](#contents)]

>The P2SH vulnerability is indeed something that needs to be fixed now that smart contracts are becoming more important on Bitcoin Cash! P2SH32 is especially important for sidechain covenants like SHA-gate which are likely to be the first contracts to manage a ton of funds.
>
>I support this effort & hope to see the ideas of fixing boolean malleability and the redeem script size limit as extension CHIPs to this one!

<a href="https://bitcoincashresearch.org/t/chip-2022-05-pay-to-script-hash-32-p2sh32-for-bitcoin-cash/806/2"><p align="right">2022-05-31, Mathieu Geukens, CashScript Developer</p></a>

>I think this is easy to be done and helpful. We'll migrate to P2SH32 when it is ready.

<a href="https://t.me/smartbchdevelopers/6902"><p align="right">2022-07-13, Kui Wang, SmartBCH Lead Developer</p></a>

>Adding P2SH32 makes sense to me as a way to make certain contracts more secure. There are some ecosystem costs associated, but I think the benefits justify these costs.

<a href="https://t.me/bch_compilers/5371"><p align="right">2022-07-16, Rosco Kalis, CashScript Maintainer</p></a>

>The solution brought forth by the P2SH32 proposal is straightforward, does not break existing software, and we believe will provide a solid foundation for GP to build our business upon.
>
>And for these reasons, as well as the very well explored rationale sections, General Protocols will like to declare our support for the CashTokens and P2SH32 proposals. We understand that minor bugs/insufficiencies may still be discovered, and we'll encourage the ecosystem to put opinions up as we approach commitment.

<a href="https://read.cash/@GeneralProtocols/general-protocols-statement-on-cashtokens-and-p2sh32-chip-proposals-for-the-may-2023-bch-upgrade-c5248f65"><p align="right">2022-11-06, General Protocols</p></a>

## Acknowledgements

[[Back to Contents](#contents)]

Thank you to the following contributors for reviewing and contributing improvements to this proposal, providing feedback, and promoting consensus among stakeholders (sorted alphabetically):
[2qx](https://bitcoincashresearch.org/u/2qx), [Andrew Stone](https://gitlab.com/gandrewstone), [Calin Culianu](https://github.com/cculianu), [freetrader](https://gitlab.com/freetrader), [imaginary_username](https://gitlab.com/im_uname), [Jason Dreyzehner](https://github.com/bitjson), [Jochen Hoenicke](https://jochen-hoenicke.de/), [John Nieri](https://gitlab.com/emergent-reasons), [Jonathan Silverblood](https://gitlab.com/monsterbitar), [Josh Green](https://github.com/joshmg), [Mark B Lundeberg](https://gitlab.com/markblundeberg), [Mathieu Geukens](https://github.com/mr-zwets), [Tom Zander](https://github.com/zander).

## Changelog

[[Back to Contents](#contents)]

This section summarizes the evolution of this document.

**2022-11-06** current 1.6.1

- spelling, add statement from General Protocols

**2022-11-05** [c615d26f](https://gitlab.com/0353F40E/p2sh32/-/commits/c615d26f18c672f019dbfe159e694265f9505ecc) 1.6.0

- improve readability, add acknowledgments section, add references to test cases
- add a note about mempool handling at time of activation (feedback from Josh Green)

**2022-09-08** [f58ecf83](https://gitlab.com/0353F40E/p2sh32/-/commits/f58ecf835f58555c9087c53af25da92a0e74534c) 1.5.2

- emphasise treatment of segwit recovery (Calin)

**2022-07-16** [2fd210ec](https://gitlab.com/0353F40E/p2sh32/-/commits/2fd210ec6f58473d75460362ba3f2b705e8c789c) 1.5.1

- add statement from Rosco Kalis

**2022-07-16** [118f4274](https://gitlab.com/0353F40E/p2sh32/-/commits/118f427429b598aef6ba3f640c2ef275451d0710) 1.5.0

- add statement from Kui Wang
- add sections:
    - Segwit recovery
    - Privacy Analysis
    - Pay-to-Public-Key-Hash (P2PKH) Outputs
    - Interaction With Derivatives of Bitcoin Cash
- wording nits

**2022-06-17** [a511446d](https://gitlab.com/0353F40E/p2sh32/-/commits/a511446da6603976f08512f91531e9b769c3562a) 1.4.0

- update external references links
- expand motivation with more examples of vulnerable contracts
- alternatives: rework sha256 discussion, add note about other hash functions
- make note of historical outputs which match the proposed p2sh32 pattern

**2022-06-08** [7c46a297](https://gitlab.com/0353F40E/p2sh32/-/commits/7c46a29703f20e9b7e318c01a8ee2592cc19a0aa) 1.3.0

- update cost&benefits summary
- collecting statements
- link back to BCR forum CHIP topic
- add one more example in favor of double hashing (feedback from Andrew Stone)
- expand on wallet requirements for accessing the feature (feedback from imaginary_username), make note of HW wallets (feedback from Jochen Hoenicke)
- remove outdated performance claim (feedback from Jason Dreyzehner)

**2022-05-30** [182e8208](https://gitlab.com/0353F40E/p2sh32/-/commits/182e8208c9bce9758850d4b572e43823a0b47eee) 1.2.0 Implement feedback from freetrader

- minor fixes and corrections
- add attack cost estimation
- mention how BTC closed the vulnerability

**2022-05-30** [ef786322](https://gitlab.com/0353F40E/p2sh32/-/commits/ef786322954412ed06636a6db8ad6041c3717251) 1.1.0 Implement feedback solicited from emergent_reasons

- clarifications and expanding some parts
- added a risk scenario (wrong wallet implementation)

**2022-05-29** [66e368be](https://gitlab.com/0353F40E/p2sh32/-/commits/66e368be8656d851f63d36e77e740a0515574fd9) 1.0.0 First Draft

## Copyright

[[Back to Contents](#contents)]

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

